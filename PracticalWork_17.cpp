﻿#include <iostream>
#include <vector>
#include <cmath>

using namespace std;
class DateClass
{
    int m_day;
    int m_month;
    int m_year;
public:
    void SetDate(int day, int month, int year)
    {
        m_day = day;
        m_month = month;
        m_year = year;
    }
public:
    void Print()
    {
        cout << "Today's date: " << m_day << "." << m_month << "." << m_year 
             << endl;
    }
};

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        double L = sqrt(x * x + y * y + z * z);
        cout << "Vector coordinates: " << x << " " << y << " " << z << endl;
        cout << "Vector length: " << L << endl;
    }

private:
    double x;
    double y;
    double z;
};

int main()
{
    DateClass Today{};
    Today.SetDate(10, 02, 2022);
    Today.Print();
    
    Vector v(10, 10, 10);
    v.Show();
    
    return 0;
}
